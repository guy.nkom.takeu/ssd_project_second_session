import { createStore } from 'vuex'

export default createStore({
  state: {
    currentUser: "",
  },
  getters: {
    currentUser: state => state.currentUser,
  },
  mutations: {
    SET_CURRENT_USER(state, username) {
      state.currentUser = username; // Add this line
    },
  },
  actions: {
  },
  modules: {
  }
})
