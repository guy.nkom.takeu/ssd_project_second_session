## project_ssd

#Members of the group(Names and matricule)

	NKOM TAKEU GUY JUNIOR :   000562542
	

##project name : agenda

#Requirements 

	- MySQL
	- Node js (version 20)
	- NPM
	- Vue js
	

#Server installation guide
	
	- Create a MySQL database locally named "expense" utf8_general_ci 
	- Pull Project_ssd project second session from the git provider
	- Open the console and cd the directory named "back_end_app"
	- In the back_end_app, in the config folder, get to the file dbConfig.js and set the user and 	password of your mysql database account
	- First run the code : npm install
	- Then Run the following command to launch the server : node index.js

#Client installation guide

	- After downloading node js and npm to your system
	- Pull Project_ssd project from the git provider 
	- Open the console and cd the directory named "front_end_app"
	- Run the following command to insall dependencies : npm install 
	- Run the following command to launch the client : npm run serve
	- Use the URL proposed to you to get access to the application on a browser

#NB
	- Launch the server before launching the client
	- The port numbers are already set in so do not change the ports 8081 for the backend and 8080 for the front end


	
	

