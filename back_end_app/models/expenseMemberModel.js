// models/expenseMemberModel.js
module.exports = (sequelize, Sequelize) => {
    const ExpenseMember = sequelize.define("expenseMembers", {
      balance: {
        type: Sequelize.FLOAT,
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: "unpaid"
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users', // name of the table, not the model
          key: 'id'
        }
      },
      expenseId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'expenses', // name of the table, not the model
          key: 'id'
        }
      }
    });
  
    return ExpenseMember;
  };