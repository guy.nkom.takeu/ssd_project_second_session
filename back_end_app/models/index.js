const config = require("../config/dbConfig.js");

/**
 * The Sequelize instance representing the database connection.
 * @type {import('sequelize').Sequelize}
 */
const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

/**
 * The database object containing Sequelize and the initialized models.
 * @typedef {Object} DB
 * @property {import('sequelize').Sequelize} Sequelize - The Sequelize instance.
 * @property {import('sequelize').Sequelize} sequelize - The Sequelize instance representing the database connection.
 * @property {string[]} ROLES - An array containing role names.
 */

/**
 * The database object containing Sequelize and the initialized models.
 * @type {DB}
 */

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

/**
 * The User model representing the "users" table in the database.
 */
db.user = require("../models/userModel.js")(sequelize, Sequelize);

/**
 * The Role model representing the "roles" table in the database.
 */
db.role = require("../models/roleModel.js")(sequelize, Sequelize);

/**
 * The Refresh token model representing the token refresh table in the database.
 */
db.refreshToken = require("../models/refreshTokensModel.js")(sequelize, Sequelize);

db.expense = require("./expenseModel.js")(sequelize, Sequelize);

db.group = require("./groupModel.js")(sequelize, Sequelize);

db.expenseMember = require("./expenseMemberModel.js")(sequelize, Sequelize);

db.groupKey = require("./groupKeyModel.js")(sequelize, Sequelize);



/**
 * Establishes a many-to-many association between User and Role models.
 */
db.role.belongsToMany(db.user, {
  through: "user_roles",
});

/**
 * Establishes a many-to-many association between Role and User models.
 */
db.user.belongsToMany(db.role, {
  through: "user_roles",
});

db.refreshToken.belongsTo(db.user, {
  foreignKey: "userId",
  targetKey: "id",
});

db.user.hasOne(db.refreshToken, {
  foreignKey: "userId",
  targetKey: "id",
});

db.user.belongsToMany(db.expense, {
  through: db.expenseMember,
  foreignKey: 'userId',
  as: 'expenses'
});

db.expense.belongsToMany(db.user, {
  through: db.expenseMember,
  foreignKey: 'expenseId',
  as: 'users'
});

db.expenseMember.belongsTo(db.user, {
  as: 'user',
  foreignKey: 'userId'
});

db.expense.hasMany(db.expenseMember, {
  foreignKey: 'expenseId',
  as: 'expenses'
});

db.expenseMember.belongsTo(db.expense, {
  foreignKey: 'expenseId',
  as: 'expense'
});

db.expense.belongsTo(db.user, {
  foreignKey: 'paidBy'
});

db.user.hasMany(db.expense, {
  foreignKey: 'paidBy'
});

db.expense.belongsTo(db.group, {
  foreignKey: 'groupId'
});

db.group.hasMany(db.expense, {
  foreignKey: 'groupId'
});

db.group.belongsToMany(db.user, {
  through: 'group_user',
  foreignKey: 'groupId',
  as: 'users'
});

db.user.belongsToMany(db.group, {
  through: 'group_user',
  foreignKey: 'userId',
  as: 'groups'
});

db.expenseMember.belongsTo(db.group, {
  foreignKey: 'groupId'
});


db.group.hasMany(db.expenseMember, {
  foreignKey: 'groupId'
});

db.expense.belongsTo(db.group, {
  foreignKey: 'groupId'
});

db.group.hasMany(db.expense, {
  foreignKey: 'groupId'
});

db.user.hasMany(db.groupKey, {
  foreignKey: 'userId',
  as: 'groupKeys'
});

db.group.hasMany(db.groupKey, {
  foreignKey: 'groupId',
  as: 'groupKeys'
});

db.groupKey.belongsTo(db.user, {
  foreignKey: 'userId',
  as: 'user'
});

db.groupKey.belongsTo(db.group, {
  foreignKey: 'groupId',
  as: 'group'
});



/**
 * An array containing predefined role names.
 * @type {string[]}
 */

db.ROLES = ["member", "admin"];

module.exports = db;
