/**
 * Define the User model for the database.
 * @function
 * @name defineUserModel
 * @param {Sequelize} sequelize - The Sequelize instance for the database connection.
 * @param {import('sequelize').DataTypes} Sequelize - The Sequelize DataTypes object.
 * @returns {import('sequelize').ModelCtor<import('sequelize').Model<any, any>>} The User model.
 */

module.exports = (sequelize, Sequelize) => {
    /**
     * @typedef {Object} UserModelAttributes
     * @property {string} username - The username of the user.
     * @property {string} email - The email of the user.
     * @property {string} password - The password of the user.
     */
    const User = sequelize.define("users", {
      username: {
        type: Sequelize.TEXT,
      },
      email: {
        type: Sequelize.TEXT,
      },
      password: {
        type: Sequelize.TEXT,
      },
      publicKey: {
        type: Sequelize.TEXT,
      },
      privateKey: { 
        type: Sequelize.TEXT,
      }
    });
  
    return User;
  };
  