const fs = require('fs');
const path = require('path');

const getLogs = (req, res) => {
  const logFilePath = path.join(__dirname, '..', 'logs', 'combined.log');

  fs.readFile(logFilePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading log file:', err);
      return res.status(500).send({ error: 'Failed to read log file' });
    }

    const logEntries = data.split('\n').filter(entry => entry.trim() !== '').map(JSON.parse);
    res.json(logEntries);
  });
};

module.exports = {
  getLogs
};