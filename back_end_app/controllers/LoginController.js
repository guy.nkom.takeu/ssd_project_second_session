const db = require("../models");
const config = require("../config/authConfig.js");
const { user: User, role: Role, refreshToken: RefreshToken } = db;
const validator = require('validator');
const logger = require("../midllewares/logger");
const crypto = require('crypto');

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

function hashPassword(password, salt, callback) {
    // The number of iterations, key length and digest algorithm can be configured as needed
    const iterations = 10000;
    const keylen = 64;
    const digest = 'sha512';
  
    crypto.pbkdf2(password, salt, iterations, keylen, digest, (err, derivedKey) => {
      if (err) {
        callback(err, null);
      } else {
        const hash = derivedKey.toString('hex');
        callback(null, { salt: salt, hash: hash });
      }
    });
  }

  function generateSalt(length = 16) {
    return crypto.randomBytes(length).toString('hex');
  }


exports.signup = (req, res) => {
  const { username, email, password, publicKey, privateKey } = req.body;
   // Check password complexity
   const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
   if (!validator.matches(password, passwordRegex)) {
     return res.status(400).send({
       message: "Password must be at least 8 characters long, and include at least one number, one uppercase letter, one lowercase letter, and one special character."
     });
   }
  // Save User to Database
  User.create({
    username: username,
    email: email,
    // password: bcrypt.hashSync(password, 8),
    password: passwordData.hash,
    salt: passwordData.salt, // Make sure your User model can store the salt
    publicKey: publicKey,
    privateKey: privateKey
  })
    .then((user) => {
      logger.info(`User ${user.username} signed up`);
      if (req.body.roles) {
        Role.findAll({
          where: {
            name: {
              [Op.or]: req.body.roles,
            },
          },
        }).then((roles) => {
          user.setRoles(roles).then(() => {
            res.send({ message: "User was registered successfully!" });
          });
        });
      } else {
        // user role = 1
        user.setRoles([1]).then(() => {
          res.send({ message: "User was registered successfully!" });
        });
      }
    })
    .catch((err) => {
      logger.error(`Signup error: ${err.message}`);
      res.status(500).send({ message: err.message });
    });
};


exports.signin = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username,
    },
  })
    .then(async (user) => {
      if (!user) {
        logger.warn(`User ${req.body.username} not found`);
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        logger.warn(`Invalid password attempt for user ${req.body.username}`);
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }

      const token = jwt.sign({ id: user.id }, config.secret, {
        algorithm: "HS256",
        allowInsecureKeySizes: true,
        expiresIn: config.jwtExpiration, // 24 hours
      });

      let refresh_token = await RefreshToken.createToken(user);

      let authorities = [];

      user.getRoles().then((roles) => {
        for (let i = 0; i < roles.length; i++) {
          authorities.push("ROLE_" + roles[i].name.toUpperCase());
        }
        res.status(200).send({
          id: user.id,
          username: user.username,
          email: user.email,
          privateKey: user.privateKey,
          roles: authorities,
          accessToken: token,
          refreshToken: refresh_token,
        });
        logger.info(`User ${user.username} logged in`);
      });
    })
    .catch((err) => {
      logger.error(`Signin error: ${err.message}`);
      res.status(500).send({ message: err.message });
    });
};


exports.refreshToken = async (req, res) => {
  const { refreshToken: requestToken } = req.body;

  if (requestToken == null) {
    return res.status(403).json({ message: "Refresh Token is required!" });
  }

  try {
    let refreshToken = await RefreshToken.findOne({
      where: { token: requestToken },
    });

    console.log(refreshToken);

    if (!refreshToken) {
      logger.warn(`Invalid refresh token attempt`);
      res.status(403).json({ message: "Refresh token is not in database!" });
      return;
    }

    if (RefreshToken.verifyExpiration(refreshToken)) {
      RefreshToken.destroy({ where: { id: refreshToken.id } });

      res.status(403).json({
        message: "Refresh token was expired. Please make a new signin request",
      });
      return;
    }

    const user = await refreshToken.getUser();
    let newAccessToken = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: config.jwtExpiration,
    });

    return res.status(200).json({
      accessToken: newAccessToken,
      refreshToken: refreshToken.token,
    });
  } catch (err) {
    logger.error(`Refresh token error: ${err.message}`);
    return res.status(500).send({ message: err });
  }
};
