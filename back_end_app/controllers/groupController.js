const db = require("../models");
const logger = require("../midllewares/logger");
const { group: Group, user: User, expenseMember: ExpenseMember  } = db;
const Op = db.Sequelize.Op;

const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
  
    return { limit, offset };
  };


  const getPagingData = (data, page, limit) => {
    const { count: totalItems, rows: group } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);
  
    return { totalItems, group, totalPages, currentPage };
  };

  exports.create = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.members || !Array.isArray(req.body.members)) {
      res.status(400).send({
        message: "Content can not be empty!",
      });
      return;
    }
  
    // Create a Group
    const group = {
      name: req.body.name,
    };
  
    // Save Group in the database
    Group.create(group)
      .then((data) => {
        const promises = req.body.members.map(userId => {
          return User.findByPk(userId)
            .then(user => {
              if (!user) {
                throw new Error(`User with id ${userId} not found`);
              }
              return data.addUser(user);
            });
        });
  
        return Promise.all(promises)
          .then(() => {
           
            res.send(data);
          })
          .catch((err) => {
            logger.error(`Group error: ${err.message}`);
            res.status(500).send({
              message: err.message || "Some error occurred while adding members to the Group.",
            });
          });
      })
      .catch((err) => {
        logger.error(`Group error: ${err.message}`);
        res.status(500).send({
          message: err.message || "Some error occurred while creating the Group.",
        });
      });
  };

  // Add members to the group
exports.addMember = (req, res) => {
  const { groupId, userId } = req.params;

  Group.findByPk(groupId)
    .then((group) => {
      if (!group) {
        res.status(404).send({ message: "Group not found" });
        return;
      }

      User.findByPk(userId)
        .then((user) => {
          if (!user) {
            res.status(404).send({ message: "User not found" });
            return;
          }

          group.addUser(user);
          res.send({ message: "User added to group successfully" });
        })
        .catch((err) => {
          res.status(500).send({
            message: err.message || "Some error occurred while adding the member to the group.",
          });
        });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while adding the member to the group.",
      });
    });
};

// Update a group
exports.update = (req, res) => {
  const id = req.params.id;

  Group.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Group was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Group with id=${id}. Maybe Group was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Group with id=" + id,
      });
    });
};

// Delete a group
exports.delete = (req, res) => {
  const id = req.params.id;

  Group.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Group was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Group with id=${id}. Maybe Group was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Group with id=" + id,
      });
    });
};


// List all groups
exports.findAll = (req, res) => {
  const { page, size, name } = req.query;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  const { limit, offset } = getPagination(page, size);

  Group.findAndCountAll({ where: condition, limit, offset })
    .then((data) => {
      const response = getPagingData(data, page, limit);
      res.send(response);
      logger.info(`The groups are retrieved`);
    })
    .catch((err) => {
      logger.error(`Group error: ${err.message}`);
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving groups.",
      });
    });
};


// List a group
exports.findOne = (req, res) => {
  const id = req.params.id;

  Group.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
        logger.info(`The groups are retrieved`);
      } else {
        
        res.status(404).send({
          message: `Cannot find Group with id=${id}.`,
        });
      }
    })
    .catch((err) => {
      logger.error(`Group error: ${err.message}`);
      res.status(500).send({
        message: "Error retrieving Group with id=" + id,
      });
    });
};

// List all members of a group
exports.findMembers = (req, res) => {
  const groupId = req.params.groupId;

  Group.findByPk(groupId)
    .then((group) => {
      if (!group) {
        res.status(404).send({ message: "Group not found" });
        return;
      }

      group.getUsers({
        attributes: { exclude: ['password'] }
      })
        .then((users) => {
          res.send(users);
        })
        .catch((err) => {
          logger.error(`Group error: ${err.message}`);
          res.status(500).send({
            message: err.message || "Some error occurred while retrieving members of the group.",
          });
        });
    })
    .catch((err) => {
      logger.error(`Group error: ${err.message}`);
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving members of the group.",
      });
    });
};

// List all groups for a specific user
exports.findUserGroups = (req, res) => {
  const userId = req.params.userId;

  User.findByPk(userId)
    .then((user) => {
      if (!user) {
        res.status(404).send({ message: "User not found" });
        return;
      }

      user.getGroups()
        .then((groups) => {
          res.send(groups);
          logger.info(`The groups are retrieved`);
        })
        .catch((err) => {
          logger.error(`Group error: ${err.message}`);
          res.status(500).send({
            message: err.message || "Some error occurred while retrieving groups of the user.",
          });
        });
    })
    .catch((err) => {
      logger.error(`Group error: ${err.message}`);
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving groups of the user.",
      });
    });
};


  



