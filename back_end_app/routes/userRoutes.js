const { authJwt } = require("../midllewares/index.js");
const controller = require("../controllers/userController.js");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
  
    app.get("/api/test/all", controller.allAccess);

    // Retrieve all groups
    app.get("/api/users", [authJwt.verifyToken], controller.findAllUsers);
  
    app.get(
      "/api/test/user",
      [authJwt.verifyToken],
      controller.userBoard
    );
  
    app.get(
      "/api/test/admin",
      [authJwt.verifyToken, authJwt.isAdmin],
      controller.adminBoard
    );
  };