const controller = require("../controllers/monitorController.js");

module.exports = function (app) {
    app.use(function (req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

// Route to get all logs
app.get("/api/logs", controller.getLogs);

};