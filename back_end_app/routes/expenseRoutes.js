const { authJwt } = require("../midllewares/index.js");
const controller = require("../controllers/expenseController.js");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

   // Create a new expense
   app.post("/api/expenses", [authJwt.verifyToken], controller.create);

   // Retrieve all expenses linked to the current user
   app.get("/api/expenses", [authJwt.verifyToken], controller.findAll);
 
   // Update an expense with id
   app.put("/api/expenses/:id", [authJwt.verifyToken], controller.update);
 
   // Delete an expense with id
   app.delete("/api/expenses/:id", [authJwt.verifyToken], controller.delete);

   // Retrieve all expenses for a particular user
  app.get("/api/expenses/user/:userId", [authJwt.verifyToken], controller.findAllByUser);

  // Retrieve all expenses created by a particular user
  app.get("/api/expenses/owner/:userId", [authJwt.verifyToken], controller.findAllByOwner);
};
