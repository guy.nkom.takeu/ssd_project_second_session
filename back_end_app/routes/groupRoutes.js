const { authJwt } = require("../midllewares/index.js");
const controller = require("../controllers/groupController.js");

module.exports = function (app) {
    app.use(function (req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

    // Create a new group
  app.post("/api/groups", [authJwt.verifyToken], controller.create);

  // Retrieve all groups
  app.get("/api/groups", [authJwt.verifyToken], controller.findAll);

  // Retrieve a single group with id
  app.get("/api/groups/:id", [authJwt.verifyToken], controller.findOne);

  // Update a group with id
  app.put("/api/groups/:id", [authJwt.verifyToken], controller.update);

  // Delete a group with id
  app.delete("/api/groups/:id", [authJwt.verifyToken], controller.delete);

  // Add a member to a group
  app.post("/api/groups/:groupId/members/:userId", [authJwt.verifyToken], controller.addMember);

  // Retrieve all members of a group
  app.get("/api/groups/:groupId/members", [authJwt.verifyToken], controller.findMembers);

  // Retrieve all groups for a specific user
  app.get("/api/users/:userId/groups", [authJwt.verifyToken], controller.findUserGroups);

};