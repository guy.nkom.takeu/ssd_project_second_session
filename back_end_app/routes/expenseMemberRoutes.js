const { authJwt } = require("../midllewares/index.js");
const controller = require("../controllers/expenseMemberController.js");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });


  // Pay balance for a specific expense
  app.put("/api/expenses/:expenseId/users/:userId/pay", [authJwt.verifyToken], controller.payBalance);

  // Consult balance per expense
  app.get("/api/expenses/:expenseId/users/:userId/balance", [authJwt.verifyToken], controller.findBalance);

  // List the history of payments
  app.get("/api/users/:userId/payments", [authJwt.verifyToken], controller.findPaymentHistory);


};