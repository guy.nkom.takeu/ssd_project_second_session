/**
 * Express application instance
 * @external express
 * @see {@link https://expressjs.com}
 */

/**
 * The express module
 * @type {external:express}
 * @namespace
 */
const express = require("express");

const https = require("https");

const fs = require("fs");

/**
 * Cross-Origin Resource Sharing middleware.
 * @external cors
 * @see {@link https://www.npmjs.com/package/cors}
 */

/**
 * The cors module.
 * @type {external:cors}
 * @namespace
 */
const cors = require("cors");

//const db = require("./entities");
const db = require("./models");

const app = express();

/**
 * CORS options for cross-origin resource sharing.
 * @typedef {Object} CorsOptions
 * @property {string} origin - The allowed origin for CORS requests.
 */

/**
 * CORS options configuration.
 * @type {CorsOptions}
 */

// var corsOptions = {
//   origin: "https://localhost:8080",
// };
var corsOptions = {
  origin: ["https://localhost:8080", "https://localhost:8082"],
};

// Enable CORS middleware with the specified options.
app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const Role = db.role;

/**
 * Synchronize the database and create initial data.
 * @function
 * @name syncDatabaseAndInitialize
 * @returns {void}
 */

db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and Resync Db");
  initial();
});

function initial() {
  /**
   * The data representing a role to be created in the database.
   * @typedef {Object} RoleData
   * @property {number} id - The unique identifier of the role.
   * @property {string} name - The name of the role.
   */

  /**
   * The data representing a role to be created in the database.
   * @type {RoleData[]}
   */

  const rolesToCreate = [
    { id: 1, name: "member" },
    { id: 2, name: "admin" }
  ];

  rolesToCreate.forEach((roleData) => {
    Role.create(roleData);
  });
}

app.get("/", (req, res) => {
  res.json({ message: "Backend running..." });
});

// routes
require("./routes/authRoutes.js")(app);
require("./routes/userRoutes.js")(app);
require("./routes/expenseRoutes.js")(app);
require("./routes/groupRoutes.js")(app);
require("./routes/expenseMemberRoutes.js")(app);
require("./routes/groupKeyRoutes.js")(app);
require("./routes/logsRoute.js")(app);

/**
 * The port number where the server listens for incoming requests.
 * @const {number}
 * @default 8080
 */

const PORT = process.env.PORT || 8081;

/**
 * Start the server and listen on the specified port.
 * @function
 * @name startServer
 * @param {number} port - The port number to listen on.
 * @returns {void}
 */

// app.listen(PORT, () => {
//   console.log(`Server is running on port ${PORT}.`);
// });

https
  .createServer(
    {
      key: fs.readFileSync("key.pem"),
      cert: fs.readFileSync("cert.pem"),
    },
    app)
  .listen(8081, ()=>{
    console.log('server is running at port 8081.');
  });
